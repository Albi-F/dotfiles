# In linux be sure to:
# + Set the right permissions:
#   - .ssh directory: 700 (drwx------)
#   - public key (.pub file): 644 (-rw-r--r--)
#   - private key (id_rsa): 600 (-rw-------)
#   - Your home directory should not be writeable by the group or others: 755 (drwxr-xr-x)
#   - Configuration file: .ssh/config: 600 (-rw-------)
# + Have both public and private keys in the .ssh directory
#   Otherwise "Passwords and Keys" will not be able to import the keys
#   and you will have to do it manually with ssh-add on every reboot
# + Private keys should follow this format:
#    -----BEGIN .................-----
#    key
#    -----END ...................-----
#    The header and footer must **not** be on the same line as the key!

############ GIT REPOSITORIES ###############
# The username must be git!!!
# It can work even if the username is wrong if the user is specified in the remote url string like this:
# git@gitlab.com:Username/repo.git
# Test these credentials with ssh -T GitXxx
Host GitLab
  User git
  Hostname gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gitlab

Host GitHub
  User git
  Hostname github.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/github

############ LUND UNIVERSITY ###############
Host UniServer
  User inf-46-2023
  Hostname bioinf-serv2.cob.lu.se
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/UniServer_ed25519
  Port 13338

Host UniPC
  User inf-46-2023
  Hostname bioinf-biol302446.biol.lu.se
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/UniPC

Host Lunarc
  User alfa
  Hostname cosmos.lunarc.lu.se
  PreferredAuthentications keyboard-interactive

############ DEFAULT CONFIGURATION ###############
# Keep the default configuration as the last item in this file becuase the first obtained value for each parameter is used.
Host *
  # Automatically add keys to a running ssh-agent (MacOS)
  UseKeychain yes
  AddKeysToAgent yes
  IdentityFile ~/.ssh/github
  IdentityFile ~/.ssh/gitlab
