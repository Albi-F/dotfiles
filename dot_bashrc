# Import aliases from another file
if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi

# Retrieve git branch

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

# The first part redirect stderr to /dev/null
# The second part use sed to extract the current branch name from the git branch output.

# Customize the prompt

# \e -> ASCII escape character
# The ASCII "escape" character is used to start a series of characters called a control sequence
# \h -> the hostname up to the first .
# \u -> The username of the current user
# \w -> Complete path of current working directory
# \[ -> begin a sequence of non-printing characters, which are used to embed a terminal control sequence into the prompt
# \] -> end a sequence of non-printing characters
# \e[0m -> Reset all attributes

# Bash manual
# https://www.gnu.org/software/bash/manual/html_node/Controlling-the-Prompt.html

# Colors and formatting
# https://misc.flogisoft.com/bash/tip_colors_and_formatting

PS1="(\[\e[34mbash\[\e[0m) \[\e[31m\]\u\[\e[39m\]@\[\e[32m\]\h\[\e[39m\]:\[\e[35m\]\w \[\e[34m\]\$(parse_git_branch)\n\[\e[0m\]> "

# Disable suspend/resume of the terminal, it conflicts with the command ctrl+s needed to go to the previous result of the bash history search
stty -ixon

# Used inside distroboxes, if the command is not found try on the host
command_not_found_handle() {
# don't run if not in a container
  if [ ! -e /run/.containerenv ] && [ ! -e /.dockerenv ]; then
    exit 127
  fi
  
  distrobox-host-exec "${@}"
}
if [ -n "${ZSH_VERSION-}" ]; then
  command_not_found_handler() {
    command_not_found_handle "$@"
 }
fi
